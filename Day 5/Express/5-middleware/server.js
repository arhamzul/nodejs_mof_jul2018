const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

var app = express();

hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs');

// use is keyword to use middleware
// to add on functionality to express
// like respond to request (like Laravel)
app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`;

  console.log(log);
  fs.appendFileSync('server.log', log + '\n');
  next(); // tell express we are done
});

// --- async append ----
// fs.appendFile('server.log', log + '\n', (err) => {
//   if (err) {
//     console.log('Unable to apppend to server.log');
//   }
// });

// ---- maintenance mode ---
// -- uncomment this to start maintenance mode
// app.use((req, res, next) => {
//   res.render('maintenance.hbs');
// });

// to avoid /help.html boleh load regardless of middleware,
// kena letak line ni bawah middleware
app.use(express.static(__dirname + '/public'));

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
});

app.get('/', (req, res) => {
  res.render('home.hbs', {
    pageTitle: 'Home Page',
    welcomeMessage: 'Welcome to my website'
  });
});

app.get('/about', (req, res) => {
  res.render('about.hbs', {
    pageTitle: 'About Page'
  });
});

// /bad - send back json with errorMessage
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle request'
  });
});

app.listen(3000, () => {
  console.log('Server is up on port 3000');
});