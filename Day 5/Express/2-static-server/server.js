const express = require('express');

var app = express();

// express middleware
// express.static takes absolute path on hard drive
// files in folder public will be loaded automatically based on url
app.use(express.static(__dirname + '/public'));
// try test
// http://localhost:3000/help.html

app.get('/', (req, res) => {
  // res.send('<h1>Hello Express!</h1>');
  res.send({
    name: 'Andrew',
    likes: [
      'Biking',
      'Cities'
    ]
  });
});

app.get('/about', (req, res) => {
  res.send('About Page');
});

// /bad - send back json with errorMessage
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle request'
  });
});

// second argument once the server is up
app.listen(3000, () => {
  console.log('Server is up on port 3000');
});