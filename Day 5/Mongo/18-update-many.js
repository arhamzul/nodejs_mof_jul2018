// Update Many Records (document)

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // Where condition untuk kita nak update
        var myquery = {
            status: 'active'
        };

        // new value
        var newvalues = {
            $set: {
                status: "Aktif"
            }
        };

        // execute
        dbo.collection("users").updateMany(myquery, newvalues, function (err, res) {
            if (err) throw err;
            console.log(res.result.nModified + " document(s) updated");
            db.close();
        });

    }
});