// inserting 1 user document (record) into users collection (table)

const mongodb = require('mongodb');
const bcrypt = require('bcrypt');


var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        // successfull connection
        // use db name mofdb
        var dbo = db.db("mofdb");

        var myobj = {
            name: "Arham Zulqarnaen",
            role: "Developer",
            email: 'arhamzul@gmail.com',
            password: bcrypt.hashSync('CyperJaya22', 10),
            status: 'active'
        };
        dbo.collection("users").insertOne(myobj, function (err, res) {
            if (err) throw err;
            console.log("1 document of user inserted");
            db.close();
        });
    }
});