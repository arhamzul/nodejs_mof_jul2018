// to host web sever, we gonna use http module

var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.end('Hello World!');
}).listen(8080);

// now try access using browser 127.0.0.1:8080
// The function passed into the http.createServer() method, will be executed when someone tries to access the computer on port 8080.