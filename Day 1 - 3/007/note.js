console.log('starting note.js');

const os = require('os');

// this will shows the content of this module.
// console.log(module);
// to let this module to be used by others, we use export

// export variable value
module.exports.mesej = "Mesej ini adalah value dari variable module note.js";

// export function
module.exports.owner = () => {
    console.log('Getting owner info');
    var user = os.userInfo();

    return user.username;
}