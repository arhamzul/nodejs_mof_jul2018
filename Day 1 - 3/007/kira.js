console.log('Kira module activated');

module.exports.isipaduKotak = (panjang, lebar, tinggi) => {
    var isipadu = panjang * lebar * tinggi;
    return isipadu.toFixed(2);
}