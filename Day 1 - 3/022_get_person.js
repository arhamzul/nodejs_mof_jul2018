console.log('Getting person....');

const fs = require('fs');

var persons = [];

debugger;

getPerson('800808101123');

// function to get person
function getPerson(ic) {
    // get list of person from file first
    // use try catch, so invalid or non exist file will not be tried to read
    try {
        personsString = fs.readFileSync('020/person.json');
        persons = JSON.parse(personsString);
    } catch (e) {
        // got error.
        console.log('Got error');
    }

    // filter person yang ic nya exactly sama dengan apa yang diberi
    var filteredPerson = persons.filter((person) => {
        return person.ic === ic;
    });


    // kalau filtered person lebih sikit dari persons original, meaning dah ada
    // person yang kena padam. So kita kena update balik ke person.json
    if (filteredPerson.length > 0) {
        console.log(filteredPerson[0]);
    } else {
        console.log('Person not found');
    }
}