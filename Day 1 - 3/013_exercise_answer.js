console.log('Answer Exercise 013 starting');

const note = require('./013/note.js');
const yargs = require('yargs');
const argv = yargs.argv;

// node 013_exercise_answer.js addNote --note="Some note here" --flag="Critical"
// node 013_exercise_answer.js listNote

var command = process.argv[2];

if (command === 'addNote') {
    note.addNote(argv.note, argv.flag)
} else if (command === 'listNote') {
    note.getAll();
} else {
    console.log('Command Not Found');
}