console.log('Listening to port 8080');
console.log('Browse : http://127.0.0.1:8080');

const http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write(`<h1>Hello World</h1>`);
    res.write(`<p>Hari ni dalam sejarah</p>`);
    res.end('');
}).listen(8080);