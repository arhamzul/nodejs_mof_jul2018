console.log('Starting app..');

const fs = require('fs'); // import file system module


// using file system to create file and append content into it
fs.appendFile('002/greeting.txt', 'Hello World', (err) => {
    if (err) {
        console.log('Unable to write to file');
    }
});

// alternatively

fs.appendFileSync('002/greeting2.txt', 'Salam');

// using file system to create directory if it is not exist
// using sync variant, meaning, it has to complete this directory creation first before 
// execute next command

var dir = '002/tmp';

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

fs.appendFileSync('002/tmp/hello.txt', 'Hello Hello MOF');