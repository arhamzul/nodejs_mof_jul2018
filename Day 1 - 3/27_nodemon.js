// install nodemon to monitor source code
// npm install -g nodemon 
// nodemon 27_nodemon.js

console.log('Installing, using nodemon and learn arrow function');

var square = (x) => {
    return x * x;
};