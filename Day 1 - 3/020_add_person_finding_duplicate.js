console.log('Adding Person without Finding duplicate');

const fs = require('fs');

var persons = [];

var person1 = {
    name: "Ali",
    ic: "800808101123",
    age: 25,
    role: "admin",
};

var person2 = {
    name: "Siti",
    ic: "990808101124",
    age: 22,
    role: "user",
};

var person3 = {
    name: "Ali",
    ic: "800808101123",
    age: 25,
    role: "admin",
};

addPerson(person1);
addPerson(person2);
addPerson(person3);

console.log(persons);

// function to add person into persons, no duplication
function addPerson(newPerson) {
    // get list of person from file first
    // use try catch, so invalid or non exist file will not be tried to read
    try {
        personsString = fs.readFileSync('020/person.json');
        persons = JSON.parse(personsString);
    } catch (e) {
        // got error.
        console.log('Got error');
    }

    console.log('Adding person...' + newPerson.name);

    // check if record already exist
    var duplicatePerson = persons.filter((person) => {
        return person.ic === newPerson.ic;
    });

    if (duplicatePerson.length === 0) {
        persons.push(newPerson);
        fs.writeFileSync('020/person.json', JSON.stringify(persons));
    }
}