// Delete generated files and folder in 002 directory

console.log('Start clean up...');

const fs = require('fs');

// syntax
// fs.unlinkSync('greeting.txt');
// fs.rmdirSync('tmp');


// EXERCISE 
// remove all files and folder generated previously, except folder 002
// ensure the file is exist before delete
// ensure the folder is exist before delete
// tips: rmdirSync only works when the directory is empty.... so ^_^
// it is best to create a dedicated function for these tasks like so...

// ------ 

// remove the files first
removeFile('002/greeting.txt');
removeFile('002/greeting2.txt');
removeFile('002/tmp/hello.txt');
removeFile('002/exercise/ucapan.txt');

// then remove the directory (if the directory is still exist)
removeFolder('002/tmp');
removeFolder('002/exercise');



// ----- ANSWER ---

// dedicated function to only attemp to remove file if they are exist
function removeFile(fileName) {
    if (fs.existsSync(fileName)) {
        fs.unlinkSync(fileName);
    }
}

// dedicated function to only attemp to remove directory if they are exist
function removeFolder(directoryName) {
    if (fs.existsSync(directoryName)) {
        fs.rmdirSync(directoryName);
    }
}

console.log('all clean up Sir');

// any simpler? use rimraf (its like rm -rf)
// https://www.npmjs.com/package/rimraf